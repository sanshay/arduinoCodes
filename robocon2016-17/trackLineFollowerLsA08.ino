//program for track line follower using LSA08

//sensor
int sensor[]={23,22,24,25,26,27,28,29};

//chasis
int leftMotorFwd = 8;
int leftMotorRev = 9;
int En = 30;
int rightMotorFwd = 10;
int rightMotorRev = 11;
int en = 8;

int sens[8];
int flag = 0;
int speed=180;
int i=0;
int count = 0;

void setup()
{
  for(i=0;i<8;i++)
  {
      pinMode(sensor[i],INPUT);
  }
  pinMode(leftMotorFwd,OUTPUT);
  pinMode(leftMotorRev,OUTPUT);
  pinMode(rightMotorFwd,OUTPUT);
  pinMode(rightMotorRev,OUTPUT);
  digitalWrite(En,HIGH);
digitalWrite(en,HIGH);
  Serial.begin(9600);
}

void loop()
{
  for(i=0;i<8;i++)
  {
      sens[i] = digitalRead(sensor[i]);
  }


  for(i=0;i<8;i++)
  {
      Serial.print(sens[i]);
      Serial.print("-");
  }
Serial.print("\n");

  //fwd (pin 3 && pin 4 reserved for center)
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 1 &&
       sens[4] == 1 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      analogWrite(leftMotorFwd,speed);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,speed);
      analogWrite(rightMotorRev,0);
  }



  //left Detect Condition 1
  else if (( sens[0] == 0  &&  sens[1] == 0  &&  sens[2] == 1)&& ( sens[5] == 0  &&  sens[6] == 0  &&  sens[7] == 0))
  {
      analogWrite(leftMotorFwd,speed);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,0);
      analogWrite(rightMotorRev,0);
  }

  //left Detect Condition 2
  else if (( sens[0] == 0  && (sens[1] == 1  &&  sens[2] == 1))&& ( sens[5] == 0  &&  sens[6] == 0  &&  sens[7] == 0))
  {
      analogWrite(leftMotorFwd,speed);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,0);
      analogWrite(rightMotorRev,0);
  }

  //left Detect Condition 3
  else if (( sens[0] == 1  &&  sens[1] == 1  &&  sens[2] == 1)&& ( sens[5] == 0  &&  sens[6] == 0  &&  sens[7] == 0))
  {
      analogWrite(leftMotorFwd,speed);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,0);
      analogWrite(rightMotorRev,0);
  }


  //right Detect Condition 1
  else if (( sens[5] == 0  &&  sens[6] == 0  &&  sens[7] == 1)&& (sens[0]==0  && sens[1]==0 && sens[2]==0 && sens[3]==0  && sens[4]==0))
  {
      analogWrite(leftMotorFwd,0);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,speed);
      analogWrite(rightMotorRev,0);
  }

  //right Detect Condition 2
  else if (( sens[5] == 0  && (sens[6] == 1  &&  sens[7] == 1))&& (sens[0]==0  && sens[1]==0 && sens[2]==0 && sens[3]==0  && sens[4]==0))
  {
      analogWrite(leftMotorFwd,0);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,speed);
      analogWrite(rightMotorRev,0);
  }

  //right Detect Condition 3
  else if (( sens[5] == 1  &&  sens[6] == 1  &&  sens[7] == 1) && (sens[0]==0  && sens[1]==0 && sens[2]==0 && sens[3]==0  && sens[4]==0))
  {
      analogWrite(leftMotorFwd,0);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,speed);
      analogWrite(rightMotorRev,0);
  }


  //cross Section Detection
  else if (
            sens[0] == 1 && sens[1] == 1 &&  sens[2] == 1 && sens[3] == 1 &&
            sens[4] == 1 && sens[5] == 1 && sens[6] == 1 &&  sens[7] == 1
          )
  {
      Serial.println("cross-section:");
      count = count + 1;
      Serial.println(count);



          analogWrite(leftMotorFwd,0);
          analogWrite(leftMotorRev,0);
          analogWrite(rightMotorFwd,0);
          analogWrite(rightMotorRev,0);
          delay(5000);


          flag =1;

   }

   if(flag == 1)
   {
          analogWrite(leftMotorFwd,speed);
          analogWrite(leftMotorRev,0);
          analogWrite(rightMotorFwd,speed);
          analogWrite(rightMotorRev,0);
          flag = 0;
   }


   if(
            sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
            sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )

   {
          analogWrite(leftMotorFwd,0);
          analogWrite(leftMotorRev,speed);
          analogWrite(rightMotorFwd,0);
          analogWrite(rightMotorRev,speed);
   }

}
