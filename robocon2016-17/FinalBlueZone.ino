//**************************************************************************
// Robocon 2017
// Team Robominions
//Dhole Patil College of Engineering Pune

//***************************************************************************

//CODE FOR BLUE ZONE

#include <PS2X_lib.h>  //for v1.6
#include<Wire.h>
#include<LiquidCrystal_I2C.h>
#include<Servo.h>

// LCD Displays to display useful information of the bot
LiquidCrystal_I2C lcd1(0x3f, 16 , 2); //top
LiquidCrystal_I2C lcd2(0x3A, 16 , 2); //bottom


// Chasis motors arranged in 90 degree position.
int frontMotorDirection = 25;
int frontMotorPWM = 5;

int leftMotorDirection = 24;
int leftMotorPWM = 4;

int backMotorDirection = 23;
int backMotorPWM = 3;

int rightMotorDirection = 22;
int rightMotorPWM = 2;

// default speed of chasis, throwing and ACL & CL motors
//int speedForLineFollower = 255;
int speedForClockAndAntiClockMotion = 60;

//Pin attached to the the pneumatic control of throwing piston.
int throwingPiston = 8;

//Pins of the throwing motor (Nischibo motors)
int throwingMotor1Dir = 41 ;
int throwingMotor1PWM = 6;
int throwingMotor2Dir = 40;
int throwingMotor2PWM = 7;

// Speed limits for the throwing motor in PWM values
int maxSpeedForThrowingMotor = 255;
int speedForThrowingMotor = 100;
int minSpeedForThrowingMotor = 0;

//Motor to control the X axis rotation( in a plane) of the throwing mechanism
int xAxisMotorPinA = 53;
int xAxisMotorPinB = 52;
//int xAxisMotorEnable = 47;


//Motor to control the Z axis vertical angle
int zAxisMotorPinA = 51;
int zAxisMotorPinB = 50;
//int zAxisMotorEnable = ;

//motor to control vertical pull up /pull down motion
// TO lift th whole mechanism up or down
int verticalMechanismPullMotorPinA = 27;
int verticalMechanismPullMotorPinB = 26;
//int verticalMechanismEnable = 45;


//line sensor LSA08
int sensor[]={49,48,47,46,45,44,43,42};
int lineFollowerCount = 0;
int i=0;
int sens[8];
//int lineFollowerFlag=0;
int crossSectionCount = 0;
int speedForLineFollower = 255;


//Y axis of Mechanism
Servo YServo;
int YServoAngle = 0;
int YServoMaxAngle = 250;
int YServoMinAngle = 0;

int flag = 0;

/******************************************************************
 * set pins connected to PS2 controller:
 *   - 1e column: original
 *   - 2e colmun: Stef?
 * replace pin numbers by the ones you use
 ******************************************************************/
#define PS2_DAT        14  //14
#define PS2_CMD        15  //15
#define PS2_SEL        16 //16
#define PS2_CLK        17 //17

/******************************************************************
 * select modes of PS2 controller:
 *   - pressures = analog reading of push-butttons
 *   - rumble    = motor rumbling
 * uncomment 1 of the lines for each mode selection
 ******************************************************************/
#define pressures   false
//#define pressures   false
#define rumble      false
//#define rumble      false
//int buttonPushCounter = 0;
PS2X ps2x; // create PS2 Controller Class

//right now, the library does NOT support hot pluggable controllers, meaning
//you must always either restart your Arduino after you connect the controller,
//or call config_gamepad(pins) again after connecting the controller.

int error = 0;
byte type = 0;
byte vibrate = 0;
//create object


//setCursor(pixel,row)


// Setting I/O modes of all Devices and there initializations
void setup()
{
  Serial.begin(57600);

  //lcd
  lcd1.begin();
  lcd2.begin();
  lcd1.backlight();
  lcd2.backlight();


  //common enable
  digitalWrite(0,HIGH);

  //line Sensor
  for(i=0;i<8;i++)
  {
      pinMode(sensor[i],INPUT);
  }


  //chasis motors of wheels arranged in 90 Degree
  pinMode(frontMotorDirection, OUTPUT);
  pinMode(frontMotorPWM, OUTPUT);

  pinMode(leftMotorDirection, OUTPUT);
  pinMode(leftMotorPWM, OUTPUT);

  pinMode(backMotorDirection, OUTPUT);
  pinMode(backMotorPWM, OUTPUT);

  pinMode(rightMotorDirection, OUTPUT);
  pinMode(rightMotorPWM, OUTPUT);

  //pneumatic for loading and pushing discs to the throwing motor wheel
  pinMode(throwingPiston, OUTPUT);

  //throwing  motors, Nischibo
  pinMode(throwingMotor1Dir, OUTPUT);
  pinMode(throwingMotor1PWM, OUTPUT);
  pinMode(throwingMotor2Dir, OUTPUT);
  pinMode(throwingMotor2PWM, OUTPUT);

  //xAxisMotor
  pinMode(xAxisMotorPinA, OUTPUT);
  pinMode(xAxisMotorPinB, OUTPUT);
  //pinMode(xAxisMotorEnable, OUTPUT);

  //zAxisMotor
  pinMode(zAxisMotorPinA, OUTPUT);
  pinMode(zAxisMotorPinB, OUTPUT);
  //pinMode(zAxisMotorEnable, OUTPUT);

  //verticalMechanismPull
  pinMode(verticalMechanismPullMotorPinA, OUTPUT);
  pinMode(verticalMechanismPullMotorPinB, OUTPUT);
  //pinMode(verticalMechanismEnable, OUTPUT);

  //mechanismTilt
  //pinMode(tiltMotorPinA,OUTPUT);
  //pinMode(tiltMotorPinB,OUTPUT);

  //Servo attach
  YServo.attach(10);
  YServo.write(0);

  delay(300);  //added delay to give wireless ps2 module some time to startup, before configuring it

  //CHANGES for v1.6 HERE!!! **************PAY ATTENTION*************

  //setup pins and settings: GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
  error = ps2x.config_gamepad(PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);

  if(error == 0)
  {
    Serial.println("Found Controller, configured successful ");
    Serial.println("pressures = ");
    if (pressures)
    Serial.println("true ");
    else
    Serial.println("false");
    Serial.println("rumble = ");
    if (rumble)
    Serial.println("true)");
    else
    Serial.println("false");
    Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;");
    Serial.println("holding L1 or R1 will print out the analog stick values.");
    Serial.println("Note: Go to www.billporter.info for updates and to report bugs.");
   }

  else if(error == 1)
  {
      Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");
      lcd1.clear();
      lcd1.setCursor(0,0);
      lcd1.print("PS2 not found");
      lcd1.setCursor(0,1);
      lcd1.print("Check Wiring");
  }

  else if(error == 2)
  {
      Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");
  }

  else if(error == 3)
  {
      Serial.println("Controller refusing to enter Pressures mode, may not support it. ");

    //  Serial.print(ps2x.Analog(1), HEX);
  }

  type = ps2x.readType();
  switch(type)
  {
    case 0:
          Serial.println("Unknown Controller type found ");
          lcd1.clear();
          lcd1.print("controller found");
          break;
    case 1:
          Serial.println("DualShock Controller found ");
          break;
    case 2:
          Serial.println("GuitarHero Controller found ");
          break;
    case 3:
          Serial.println("Wireless Sony DualShock Controller found ");
          break;
   }
}





//execution starts here !!

void loop()
{
  /* You must Read Gamepad to get new values and set vibration values
     ps2x.read_gamepad(small motor on/off, larger motor strenght from 0-255)
     if you don't enable the rumble, use ps2x.read_gamepad(); with no values
     You should call this at least once a second
   */
  if(error == 1) //skip loop if no controller found
    return;

  if(type == 2)
  {
    Serial.println("Guitar Hero Controller not being used");
  }
  else
  {
    //DualShock Controller
    ps2x.read_gamepad(false, vibrate); //read controller and set large motor to spin at 'vibrate' speed







    if (flag == 0)
    {
        int  xn = map(ps2x.Analog(PSS_LX),0,127,-127,0);
        int  xp = map(ps2x.Analog(PSS_LX),128,255,0,127);

        int  yn = map(ps2x.Analog(PSS_LY),128,255,0,-127);
        int  yp = map(ps2x.Analog(PSS_LY),0,127,127,0);


        analogRead(xn);
        analogRead(xp);
        analogRead(yn);
        analogRead(yp);

        Serial.print(xn);
        Serial.print(",");

        Serial.print(yp);
        Serial.print(",");

        Serial.print(xp);
        Serial.print(",");

        Serial.print(yn);
        Serial.println(" ");

        if(xn > -50 && xp < 50 && yp < 50 && yn > -50 )  // STOP
        {
          digitalWrite(frontMotorDirection,HIGH);
          analogWrite(frontMotorPWM,0);

          digitalWrite(backMotorDirection,HIGH);
          analogWrite(backMotorPWM,0);

          digitalWrite(leftMotorDirection,HIGH);
          analogWrite(leftMotorPWM,0);

          digitalWrite(rightMotorDirection,HIGH);
          analogWrite(rightMotorPWM,0);
        }

         else if(yp > 50  && xp < 50 && xn>-50)  //TOP
        {
          digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
          analogWrite(frontMotorPWM, 0);

          digitalWrite(leftMotorDirection, HIGH);
          analogWrite(leftMotorPWM, yp);

          digitalWrite(backMotorDirection, LOW);
          analogWrite(backMotorPWM, 0);

          digitalWrite(rightMotorDirection, LOW);
          analogWrite(rightMotorPWM, yp);
        }

        else if(xn < -50 && yp > 50) // TOP LEFT
        {
          digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
          analogWrite(frontMotorPWM, abs(xn));

          digitalWrite(leftMotorDirection, HIGH);
          analogWrite(leftMotorPWM, yp);

          digitalWrite(backMotorDirection, HIGH);
          analogWrite(backMotorPWM, abs(xn));

          digitalWrite(rightMotorDirection, LOW);
          analogWrite(rightMotorPWM, yp);

        }

        else if( yp > 50 && xp > 50) // TOP RIGHT
        {
          digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
          analogWrite(frontMotorPWM, abs(xp));

          digitalWrite(leftMotorDirection, HIGH);
          analogWrite(leftMotorPWM, yp);

          digitalWrite(backMotorDirection, LOW);
          analogWrite(backMotorPWM, abs(xp));

          digitalWrite(rightMotorDirection, LOW);
          analogWrite(rightMotorPWM, yp);
        }

        else if(yn>-50 && yp < 50 && xn <-50) //LEFT
        {
          digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
          analogWrite(frontMotorPWM, (2*abs(xn)-20));

          digitalWrite(leftMotorDirection, HIGH);
          analogWrite(leftMotorPWM, 0);

          digitalWrite(backMotorDirection, HIGH);
          analogWrite(backMotorPWM, (2*abs(xn)-40));

          digitalWrite(rightMotorDirection, LOW);
          analogWrite(rightMotorPWM, 0);
        }

        else if (yp<50 && yn>-50 && xp>50) // RIGHT
        {
          digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
          analogWrite(frontMotorPWM, (2*abs(xp)-20));

          digitalWrite(leftMotorDirection, HIGH);
          analogWrite(leftMotorPWM, 0);

          digitalWrite(backMotorDirection, LOW);
          analogWrite(backMotorPWM, (2*abs(xp)-40));

          digitalWrite(rightMotorDirection, LOW);
          analogWrite(rightMotorPWM, 0);

        }

        else if (yn < -50 && xp < 50 && xn > -50) // BOTTOM
        {
          digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
          analogWrite(frontMotorPWM, 0);

          digitalWrite(leftMotorDirection, LOW);
          analogWrite(leftMotorPWM, yp);

          digitalWrite(backMotorDirection, LOW);
          analogWrite(backMotorPWM, 0);

          digitalWrite(rightMotorDirection, HIGH);
          analogWrite(rightMotorPWM, yp);

        }

        else if ( xn < -50 && yn < -50) //BOTTOM LEFT
        {
          digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
          analogWrite(frontMotorPWM, abs(xn));

          digitalWrite(leftMotorDirection, LOW);
          analogWrite(leftMotorPWM, abs(yn));

          digitalWrite(backMotorDirection, HIGH);
          analogWrite(backMotorPWM, abs(xn));

          digitalWrite(rightMotorDirection, HIGH);
          analogWrite(rightMotorPWM, abs(yn));
        }


        else if ( xp > 50 && yn <- 50) //BOTTOM RIGHT
        {
          digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
          analogWrite(frontMotorPWM, abs(xp));

          digitalWrite(leftMotorDirection, LOW);
          analogWrite(leftMotorPWM, abs(yn));

          digitalWrite(backMotorDirection, LOW);
          analogWrite(backMotorPWM, abs(xp));

          digitalWrite(rightMotorDirection, HIGH);
          analogWrite(rightMotorPWM, abs(yn));

        }


        int  xrn = map(ps2x.Analog(PSS_RX),0,127,-127,0);
        int  xrp = map(ps2x.Analog(PSS_RX),128,255,0,127);

        int  yrn = map(ps2x.Analog(PSS_RY),128,255,0,-127);
        int  yrp = map(ps2x.Analog(PSS_RY),0,127,127,0);



        if(xn > -50 && xp < 50 && yp < 50 && yn > -50 )  // STOP
        {
           allStop();
        }


        if(yrp > 50  && xrp < 50 && xrn>-50)  //TOP
        {
          verticalMechanismPullUp();
        }


        else if (yrn < -50 && xrp < 50 && xrn > -50) // BOTTOM
        {
          verticalMechanismPullDown();
        }

        else if (yrp<50 && yrn>-50 && xrp>50) // RIGHT
        {
          zAxisClockwiseTilt();
        }

        else if (yrn>-50 && yp < 50 && xrn <-50) //LEFT
        {
          zAxisAntiClockwiseTilt();
        }


      }//flag == 0


      if(ps2x.Button(PSB_L3))
      {
        Serial.println("L3 pressed");
      }

      if(ps2x.Button(PSB_R3))
      {
        Serial.println("R3 pressed");
      }

      if(ps2x.Button(PSB_L2))
      {
        Serial.println("L2 pressed");
        wholeBotAntiClockwise();
      }

      if(ps2x.Button(PSB_R1))
      {
        Serial.println("R1 pressed");
        throwingMotorSpeedIncrease();
      }

      if(ps2x.Button(PSB_L1))
      {
        Serial.println("L1 pressed");
        throwingMotorSpeedDecrease();
      }

      if(ps2x.Button(PSB_R2))
      {
        Serial.println("R2 pressed");
        Serial.println(ps2x.Analog(PSAB_R2), DEC);
        wholeBotClockwise();
      }


      if(ps2x.ButtonPressed(PSB_TRIANGLE))
      {
        Serial.println("Triangle pressed");
        digitalWrite(throwingPiston,LOW);
      }

      if(ps2x.ButtonReleased(PSB_TRIANGLE))
      {
        Serial.println("Triangle pressed");
        digitalWrite(throwingPiston,HIGH);
      }

      if(ps2x.ButtonPressed(PSB_PAD_UP))
      {
        Serial.println("up pressed");
        mechanismTiltUp();
      }

      if(ps2x.ButtonReleased(PSB_PAD_UP))
      {
        Serial.println("Servo Ruk gaya");
      }


      if(ps2x.Button(PSB_PAD_LEFT))
      {
        Serial.println("left pressed");
        xAxisMotorNegativeTurn();
      }

      if(ps2x.ButtonPressed(PSB_PAD_DOWN))
      {
        Serial.println("down pressed");
        mechanismTiltDown();
      }

      if(ps2x.ButtonReleased(PSB_PAD_DOWN))
      {
          Serial.println("Servo Ruk gaya");
      }


      if(ps2x.Button(PSB_PAD_RIGHT))
      {
        Serial.println("right pressed");
        xAxisMotorPositiveTurn();
      }

      // Line follower towards right side.
      if(ps2x.ButtonPressed(PSB_CIRCLE))
      {               //will be TRUE if button was JUST pressed
        Serial.println("Circle just pressed");
        lcd1.clear();
        lcd1.print("Line Follower");
        flag = 2;
      }

      if(ps2x.Button(PSB_CROSS))
      {               //will be TRUE if button was JUST pressed OR released
        Serial.println("X just changed");
        lcd1.clear();
        lcd1.print("MANUAL");
        allStop();
      }

      //Line Follower towards left side
      if(ps2x.ButtonPressed(PSB_SQUARE))
      {              //will be TRUE if button was JUST released
        Serial.println("Square just released");
        lcd1.clear();
        lcd1.print("Line Follower");
        flag = 3;

      }

      if (flag == 2)
      {
        lineSensorRightMovement();
      }

      if (flag == 3)
      {
        lineSensorLeftMovement();
      }


  }
}


// All Individual functions are defined below





void wholeBotClockwise( )
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(frontMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(backMotorDirection, HIGH);
  analogWrite(backMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(rightMotorDirection, HIGH);
  analogWrite(rightMotorPWM, speedForClockAndAntiClockMotion);

  delay(250);
}

void wholeBotAntiClockwise( )
{
  digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(frontMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(leftMotorDirection, LOW);
  analogWrite(leftMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, speedForClockAndAntiClockMotion);

  delay(250);
}


void allStop()
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(frontMotorPWM, 0);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, 0);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);

  digitalWrite(xAxisMotorPinA,LOW);
  digitalWrite(xAxisMotorPinB,LOW);

  digitalWrite(zAxisMotorPinA,LOW);
  digitalWrite(zAxisMotorPinB,LOW);

  digitalWrite(verticalMechanismPullMotorPinA,LOW);
  digitalWrite(verticalMechanismPullMotorPinB,LOW);
  //analogWrite(verticalMechanismEnable,0);

  flag = 0;

  delay(200);
}


void zAxisAntiClockwiseTilt()
{
  digitalWrite(zAxisMotorPinA, LOW);
  digitalWrite(zAxisMotorPinB, HIGH);
  delay(100);
  digitalWrite(zAxisMotorPinA, LOW);
  digitalWrite(zAxisMotorPinB, LOW);
}


void zAxisClockwiseTilt()
{
  digitalWrite(zAxisMotorPinA, HIGH);
  digitalWrite(zAxisMotorPinB, LOW);
  delay(100);
  digitalWrite(zAxisMotorPinA, LOW);
  digitalWrite(zAxisMotorPinB, LOW);
}


void verticalMechanismPullUp()
{
  digitalWrite(verticalMechanismPullMotorPinA,HIGH);
  digitalWrite(verticalMechanismPullMotorPinB,LOW);
  //analogWrite(verticalMechanismEnable,240);
  delay(800);
  digitalWrite(verticalMechanismPullMotorPinA,LOW);
  digitalWrite(verticalMechanismPullMotorPinB,LOW);
  //analogWrite(verticalMechanismEnable,0);
}

void verticalMechanismPullDown()
{
  digitalWrite(verticalMechanismPullMotorPinA,LOW);
  digitalWrite(verticalMechanismPullMotorPinB,HIGH);
  //analogWrite(verticalMechanismEnable,240);
  delay(800);
  digitalWrite(verticalMechanismPullMotorPinA,LOW);
  digitalWrite(verticalMechanismPullMotorPinB,LOW);
  //analogWrite(verticalMechanismEnable,0);
}


void throwingMotorSpeedIncrease()
{
  digitalWrite(throwingMotor1Dir,HIGH);
  analogWrite(throwingMotor1PWM, speedForThrowingMotor);
  digitalWrite(throwingMotor2Dir,HIGH);
  analogWrite(throwingMotor2PWM, speedForThrowingMotor);

  speedForThrowingMotor = speedForThrowingMotor + 5;

  if (speedForThrowingMotor > maxSpeedForThrowingMotor)
  {
    speedForThrowingMotor = maxSpeedForThrowingMotor;
    Serial.println("Max possible speed is reached, the speed now is 255");
    lcd2.clear();
    lcd2.print("PWM 255");

  }
  Serial.println(speedForThrowingMotor);
  lcd2.clear();
  lcd2.setCursor(0,0);
  lcd2.print("PWM");
  lcd2.setCursor(0,1);
  lcd2.print(speedForThrowingMotor);

}

void throwingMotorSpeedDecrease()
{
  speedForThrowingMotor = speedForThrowingMotor - 5 ;
    if (speedForThrowingMotor < minSpeedForThrowingMotor)
  {
    speedForThrowingMotor = minSpeedForThrowingMotor;
    Serial.println("min possible speed is reached, the motors are now turned off");
    lcd2.clear();
    lcd2.print("PWM 0");

  }
  digitalWrite(throwingMotor1Dir,HIGH);
  analogWrite(throwingMotor1PWM, speedForThrowingMotor);
  digitalWrite(throwingMotor2Dir,HIGH);
  analogWrite(throwingMotor2PWM, speedForThrowingMotor);

  Serial.println(speedForThrowingMotor);

  lcd2.clear();
  lcd2.setCursor(0,0);
  lcd2.print("PWM");
  lcd2.setCursor(0,1);
  lcd2.print(speedForThrowingMotor);
}


void xAxisMotorNegativeTurn()
{
  //digitalWrite(xAxisMotorEnable, HIGH);
  digitalWrite(xAxisMotorPinA, LOW);
  digitalWrite(xAxisMotorPinB, HIGH);
  delay(50);
  //digitalWrite(xAxisMotorEnable, LOW);
  digitalWrite(xAxisMotorPinA, LOW);
  digitalWrite(xAxisMotorPinB, LOW);

}

void xAxisMotorPositiveTurn()
{
  //digitalWrite(xAxisMotorEnable, HIGH);
  digitalWrite(xAxisMotorPinA, HIGH);
  digitalWrite(xAxisMotorPinB, LOW);
  delay(50);
  //digitalWrite(xAxisMotorEnable, LOW);
  digitalWrite(xAxisMotorPinA, LOW);
  digitalWrite(xAxisMotorPinB, LOW);

}

void mechanismTiltUp()
{
  YServo.write(YServoAngle);
  lcd1.clear();
  lcd1.setCursor(0,0);
  lcd1.print("Angle of Y Servo");
  lcd1.setCursor(0,1);
  lcd1.print(YServoAngle);
  YServoAngle = YServoAngle + 10;

  if (YServoAngle > YServoMaxAngle)
  {
    YServoAngle = YServoMaxAngle;
    Serial.println("Max Angle 150 Reached");
    lcd1.clear();
    lcd1.print("Max Angle Reached");

  }
}

void mechanismTiltDown()
{
  YServoAngle - 10;
  YServo.write(YServoAngle);
  lcd1.clear();
  lcd1.setCursor(0,0);
  lcd1.print("Angle of Y Servo");
  lcd1.setCursor(0,1);
  lcd1.print(YServoAngle);

  YServoAngle = YServoAngle - 10;

  if (YServoAngle < YServoMinAngle)
  {
    YServoAngle = YServoMinAngle;
    Serial.println("Min Angle 0 Reached");
    lcd1.clear();
    lcd1.print("min Angle Reached");

  }

}

void readLineSensor()
{
  for(i=0;i<8;i++)
  {
      sens[i] = digitalRead(sensor[i]);
  }


  for(i=0;i<8;i++)
  {
      Serial.print(sens[i]);
      Serial.print("-");
  }
  Serial.print("\n");
}

void lineSensorRightMovement()
{

  readLineSensor();

  //line detect
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 1 &&
       sens[4] == 1 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(frontMotorDirection,HIGH);
      analogWrite(frontMotorPWM,speedForLineFollower);
      digitalWrite(backMotorDirection,LOW);
      analogWrite(backMotorPWM,speedForLineFollower);
      digitalWrite(leftMotorDirection,HIGH);
      analogWrite(leftMotorPWM,0);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,0);
  }

  //line detect 1
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 1 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(frontMotorDirection,HIGH);
      analogWrite(frontMotorPWM,speedForLineFollower);
      digitalWrite(backMotorDirection,LOW);
      analogWrite(backMotorPWM,speedForLineFollower);
      digitalWrite(leftMotorDirection,HIGH);
      analogWrite(leftMotorPWM,0);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,0);
  }

  //line detect 2
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 1 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(frontMotorDirection,HIGH);
      analogWrite(frontMotorPWM,speedForLineFollower);
      digitalWrite(backMotorDirection,LOW);
      analogWrite(backMotorPWM,speedForLineFollower);
      digitalWrite(leftMotorDirection,HIGH);
      analogWrite(leftMotorPWM,0);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,0);
  }

  //left sensor detect slight turn
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 1 && sens[3] == 1 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,HIGH);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,LOW);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //left sensor detect slight turn
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 1 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,HIGH);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,LOW);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }


  //left sensor detect slight turn
  if (
       sens[0] == 0 && sens[1] == 1 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
        digitalWrite(leftMotorDirection,HIGH);
        analogWrite(leftMotorPWM,speedForLineFollower-20);
        digitalWrite(rightMotorDirection,LOW);
        analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //left sensor detect slight turn
  if (
       sens[0] == 1 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
        digitalWrite(leftMotorDirection,HIGH);
        analogWrite(leftMotorPWM,speedForLineFollower-20);
        digitalWrite(rightMotorDirection,LOW);
        analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //left sensor detect med turn
  if (
         sens[0] == 0 && sens[1] == 1 &&  sens[2] == 1 && sens[3] == 0 &&
         sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
      )
  {
        digitalWrite(leftMotorDirection,HIGH);
        analogWrite(leftMotorPWM,speedForLineFollower-10);
        digitalWrite(rightMotorDirection,LOW);
        analogWrite(rightMotorPWM,speedForLineFollower-10);
  }


  //left sensor detect med turn
  if (
         sens[0] == 1 && sens[1] == 1 &&  sens[2] == 0 && sens[3] == 0 &&
         sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
      )
  {
        digitalWrite(leftMotorDirection,HIGH);
        analogWrite(leftMotorPWM,speedForLineFollower-10);
        digitalWrite(rightMotorDirection,LOW);
        analogWrite(rightMotorPWM,speedForLineFollower-10);
  }

  //right sensor detect slight turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 1 && sens[5] == 1 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //right sensor detect slight turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 1 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //right sensor detect slight turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 1 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //right sensor detect slight turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 1
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //right sensor detect med turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 1 && sens[6] == 1 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-10);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-10);
  }

  //right sensor detect med turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 1 &&  sens[7] == 1
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-10);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-10);
  }

  //no detection
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,0);
      digitalWrite(rightMotorDirection,LOW);
      analogWrite(rightMotorPWM,0);
      digitalWrite(frontMotorDirection,LOW);
      analogWrite(frontMotorPWM,0);
      digitalWrite(backMotorDirection,LOW);
      analogWrite(backMotorPWM,0);
  }


  /*//cross-section detection
  else if (
       sens[0] == 1 && sens[1] == 1 &&  sens[2] == 1 && sens[3] == 1 &&
       sens[4] == 1 && sens[5] == 1 && sens[6] == 1 &&  sens[7] == 1
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,0);
      digitalWrite(rightMotorDirection,LOW);
      analogWrite(rightMotorPWM,0);
      digitalWrite(frontMotorDirection,LOW);
      analogWrite(frontMotorPWM,0);
      digitalWrite(backMotorDirection,LOW);
      analogWrite(backMotorPWM,0);
      flag=0;
  }
  */

  //HIGH =clockwise
  //LOW = anti-clockwise

  //sens[3] && sens[4] center

  //line Detect


}




void lineSensorLeftMovement()
{

  readLineSensor();

  //line detect
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 1 &&
       sens[4] == 1 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(frontMotorDirection,LOW);
      analogWrite(frontMotorPWM,speedForLineFollower);
      digitalWrite(backMotorDirection,HIGH);
      analogWrite(backMotorPWM,speedForLineFollower);
      digitalWrite(leftMotorDirection,HIGH);
      analogWrite(leftMotorPWM,0);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,0);
  }

  //line detect 1
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 1 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(frontMotorDirection,LOW);
      analogWrite(frontMotorPWM,speedForLineFollower);
      digitalWrite(backMotorDirection,HIGH);
      analogWrite(backMotorPWM,speedForLineFollower);
      digitalWrite(leftMotorDirection,HIGH);
      analogWrite(leftMotorPWM,0);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,0);
  }

  //line detect 2
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 1 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(frontMotorDirection,LOW);
      analogWrite(frontMotorPWM,speedForLineFollower);
      digitalWrite(backMotorDirection,HIGH);
      analogWrite(backMotorPWM,speedForLineFollower);
      digitalWrite(leftMotorDirection,HIGH);
      analogWrite(leftMotorPWM,0);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,0);
  }

  //left sensor detect slight turn
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 1 && sens[3] == 1 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,HIGH);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,LOW);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //left sensor detect slight turn
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 1 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,HIGH);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,LOW);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }


  //left sensor detect slight turn
  if (
       sens[0] == 0 && sens[1] == 1 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
        digitalWrite(leftMotorDirection,HIGH);
        analogWrite(leftMotorPWM,speedForLineFollower-20);
        digitalWrite(rightMotorDirection,LOW);
        analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //left sensor detect slight turn
  if (
       sens[0] == 1 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
        digitalWrite(leftMotorDirection,HIGH);
        analogWrite(leftMotorPWM,speedForLineFollower-20);
        digitalWrite(rightMotorDirection,LOW);
        analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //left sensor detect med turn
  if (
         sens[0] == 0 && sens[1] == 1 &&  sens[2] == 1 && sens[3] == 0 &&
         sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
      )
  {
        digitalWrite(leftMotorDirection,HIGH);
        analogWrite(leftMotorPWM,speedForLineFollower-10);
        digitalWrite(rightMotorDirection,LOW);
        analogWrite(rightMotorPWM,speedForLineFollower-10);
  }


  //left sensor detect med turn
  if (
         sens[0] == 1 && sens[1] == 1 &&  sens[2] == 0 && sens[3] == 0 &&
         sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
      )
  {
        digitalWrite(leftMotorDirection,HIGH);
        analogWrite(leftMotorPWM,speedForLineFollower-10);
        digitalWrite(rightMotorDirection,LOW);
        analogWrite(rightMotorPWM,speedForLineFollower-10);
  }

  //right sensor detect slight turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 1 && sens[5] == 1 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //right sensor detect slight turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 1 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //right sensor detect slight turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 1 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //right sensor detect slight turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 1
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-20);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-20);
  }

  //right sensor detect med turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 1 && sens[6] == 1 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-10);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-10);
  }

  //right sensor detect med turn
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 1 &&  sens[7] == 1
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,speedForLineFollower-10);
      digitalWrite(rightMotorDirection,HIGH);
      analogWrite(rightMotorPWM,speedForLineFollower-10);
  }

  //no detection
  else if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
       sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,0);
      digitalWrite(rightMotorDirection,LOW);
      analogWrite(rightMotorPWM,0);
      digitalWrite(frontMotorDirection,LOW);
      analogWrite(frontMotorPWM,0);
      digitalWrite(backMotorDirection,LOW);
      analogWrite(backMotorPWM,0);
  }

/*
  //cross-section detection
  else if (
       sens[0] == 1 && sens[1] == 1 &&  sens[2] == 1 && sens[3] == 1 &&
       sens[4] == 1 && sens[5] == 1 && sens[6] == 1 &&  sens[7] == 1
     )
  {
      digitalWrite(leftMotorDirection,LOW);
      analogWrite(leftMotorPWM,0);
      digitalWrite(rightMotorDirection,LOW);
      analogWrite(rightMotorPWM,0);
      digitalWrite(frontMotorDirection,LOW);
      analogWrite(frontMotorPWM,0);
      digitalWrite(backMotorDirection,LOW);
      analogWrite(backMotorPWM,0);
      crossSectionCount = crossSectionCount + 1;

      if (crossSectionCount == 1)
      {
        lcd1.clear();
        lcd1.print("CROSS SECTION 1");
        Serial.print(crossSectionCount);
        //crossSection1();
      }

      if (crossSectionCount == 2)
      {
        lcd1.clear();
        lcd1.print("CROSS SECTION 2");
        Serial.print(crossSectionCount);
        //crossSection2();
      }

      if (crossSectionCount == 3)
      {
        lcd1.clear();
        lcd1.print("CROSS SECTION 3");
        Serial.print(crossSectionCount);
        //crossSection3();
      }

      if (crossSectionCount == 4)
      {
        lcd1.clear();
        lcd1.print("CROSS SECTION 4");
        Serial.print(crossSectionCount);
        //crossSection4();
      }

      if (crossSectionCount == 5)
      {
        lcd1.clear();
        lcd1.print("CROSS SECTION 5");
        Serial.print(crossSectionCount);
        //crossSection5();
      }


      Serial.println(crossSectionCount);
      flag = 0;
  }


  //HIGH =clockwise
  //LOW = anti-clockwise

  //sens[3] && sens[4] center

  //line Detect
*/

}



//cross section automation


/*
void crossSection1()
{
  for(int i=0;i<2;i++)
  {
    digitalWrite(zAxisMotorPinA, LOW);
    digitalWrite(zAxisMotorPinB, HIGH);
    delay(20);
    digitalWrite(zAxisMotorPinA, LOW);
    digitalWrite(zAxisMotorPinB, LOW);
  }
}



void crossSection2()
{
  for(int i=0;i<2;i++)
  {
    digitalWrite(zAxisMotorPinA, LOW);
    digitalWrite(zAxisMotorPinB, HIGH);
    delay(20);
    digitalWrite(zAxisMotorPinA, LOW);
    digitalWrite(zAxisMotorPinB, LOW);
  }
}

void crossSection3()
{
  for(int i=0;i<2;i++)
  {
    digitalWrite(zAxisMotorPinA, LOW);
    digitalWrite(zAxisMotorPinB, HIGH);
    delay(20);
    digitalWrite(zAxisMotorPinA, LOW);
    digitalWrite(zAxisMotorPinB, LOW);
  }
}


void crossSection4()
{
  for(int i=0;i<2;i++)
  {
    digitalWrite(zAxisMotorPinA, LOW);
    digitalWrite(zAxisMotorPinB, HIGH);
    delay(20);
    digitalWrite(zAxisMotorPinA, LOW);
    digitalWrite(zAxisMotorPinB, LOW);
  }
}

void crossSection5()
{
  for(int i=0;i<2;i++)
  {
    digitalWrite(zAxisMotorPinA, LOW);
    digitalWrite(zAxisMotorPinB, HIGH);
    delay(20);
    digitalWrite(zAxisMotorPinA, LOW);
    digitalWrite(zAxisMotorPinB, LOW);
  }
}
*/
