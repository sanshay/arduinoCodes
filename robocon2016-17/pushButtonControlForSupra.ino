#include <Servo.h>

Servo myservo;  // create servo object to control a servo

int potPin = A0;  // analog pin used to connect the potentiometer
int val; // To store values from pot and feed to servo
int pushButtonPin = 10;
boolean pushButtonState; // Acts as a Flag to store a position of potentiometer
int servoMotorPin = 9;


void setup()
{

  myservo.attach(servoMotorPin);
  pinMode(pushButtonPin, INPUT);
  myservo.write(0);

}

void loop()
{
  pushButtonState = digitalRead(buttonPin);

  if (buttonState == LOW){
    digitalWrite(motorPin,LOW);
    }
  else{
    digitalWrite(motorPin,HIGH);
  }

  val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
  val = map(val, 0, 1023, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
  myservo.write(val);                  // sets the servo position according to the scaled value
  delay(0);                           // waits for the servo to get there
}
