#include<Wire.h>

int topLeftMotorDirection = 24;
int topLeftMotorPWM = 4;

int bottomLeftMotorDirection = 25;
int bottomLeftMotorPWM = 5;

int bottomRightMotorDirection = 23;
int bottomRightMotorPWM = 3;

int topRightMotorDirection = 22;
int topRightMotorPWM = 2;

int speed = 240;


void setup()
{
  pinMode(topLeftMotorDirection, OUTPUT);
  pinMode(topLeftMotorPWM, OUTPUT);

  pinMode(bottomLeftMotorDirection, OUTPUT);
  pinMode(bottomLeftMotorPWM, OUTPUT);

  pinMode(topRightMotorDirection, OUTPUT);
  pinMode(topRightMotorPWM, OUTPUT);

  pinMode(bottomRightMotorDirection, OUTPUT);
  pinMode(bottomRightMotorPWM, OUTPUT);

  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  Serial.begin(115200);

}


void loop( )
{

  delay(100); // Wait for checking of transmission connection

}

void receiveEvent(int howMany)
{
    while(1 < Wire.available() ) // Necessary for reading accurate values from Due
    {

      char c  = Wire.read();
      Serial.println(c);
    }

    int x = Wire.read();
//    Serial.print(x);



    if (x == 10)
    {
      upMovement();
      Serial.println("Moving Forward");
    }
    else if(x == 20)
    {
      leftMovement();
      Serial.println("Moving Left");
    }
    else if(x == 30)
    {
      downMovement();
      Serial.println("Moving Reverse");
    }
    else if(x == 40)
    {
      rightMovement();
      Serial.println("Moving Right");
    }
    else if(x == 50)
    {
      allStop();
      Serial.println("Stopping Everything");
    }
}




void leftMovement()
{
  digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(topLeftMotorPWM, speed);

  digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
  analogWrite(bottomLeftMotorPWM, speed);

  digitalWrite(topRightMotorDirection, HIGH); //
  analogWrite(topRightMotorPWM, speed);

  digitalWrite(bottomRightMotorDirection, HIGH);
  analogWrite(bottomRightMotorPWM, speed);

}


void rightMovement()
{

    digitalWrite(topLeftMotorDirection,HIGH); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, speed);

    digitalWrite(bottomLeftMotorDirection, LOW); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, speed);

    digitalWrite(topRightMotorDirection, LOW); //
    analogWrite(topRightMotorPWM, speed);

    digitalWrite(bottomRightMotorDirection, LOW);
    analogWrite(bottomRightMotorPWM, speed);
}


void upMovement()
{

    digitalWrite(topLeftMotorDirection,HIGH); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, speed);

    digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, speed);

    digitalWrite(topRightMotorDirection, HIGH); //
    analogWrite(topRightMotorPWM, speed);

    digitalWrite(bottomRightMotorDirection, LOW);
    analogWrite(bottomRightMotorPWM, speed);
}

void downMovement()
{

    digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, speed);

    digitalWrite(bottomLeftMotorDirection, LOW); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, speed);

    digitalWrite(topRightMotorDirection, LOW); //
    analogWrite(topRightMotorPWM, speed);

    digitalWrite(bottomRightMotorDirection, HIGH);
    analogWrite(bottomRightMotorPWM, speed);
}


void allStop()
{


    digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, 0);

    digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, 0);

    digitalWrite(topRightMotorDirection, LOW); //
    analogWrite(topRightMotorPWM, 0);

    digitalWrite(bottomRightMotorDirection, HIGH);
    analogWrite(bottomRightMotorPWM, 0);
}


void wholeBotClockwise()
{

      digitalWrite(topLeftMotorDirection,HIGH); // Left Top Motor clockwise
      analogWrite(topLeftMotorPWM, speed);

      digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
      analogWrite(bottomLeftMotorPWM, speed);

      digitalWrite(topRightMotorDirection, HIGH); //
      analogWrite(topRightMotorPWM, speed);

      digitalWrite(bottomRightMotorDirection, HIGH);
      analogWrite(bottomRightMotorPWM, speed);

}

void wholeBotantiClockwise()
{

      digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
      analogWrite(topLeftMotorPWM, speed);

      digitalWrite(bottomLeftMotorDirection, LOW); // Left Bottom Motor antiClockwise
      analogWrite(bottomLeftMotorPWM, speed);

      digitalWrite(topRightMotorDirection, LOW); //
      analogWrite(topRightMotorPWM, speed);

      digitalWrite(bottomRightMotorDirection, LOW);
      analogWrite(bottomRightMotorPWM, speed);
}
