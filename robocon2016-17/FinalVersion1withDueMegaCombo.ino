#include<Wire.h>
int topLeftMotorDirection = 24;
int topLeftMotorPWM = 4;

int bottomLeftMotorDirection = 23;
int bottomLeftMotorPWM = 3;

int bottomRightMotorDirection = 25;
int bottomRightMotorPWM = 6;

int topRightMotorDirection = 22;
int topRightMotorPWM = 7;
/*
int speed = 100;
int speedForTopRight = speed;
int speedForBottomRight = speed;
int speedForTopLeft = speed;
int speedForBottomLeft = speed;
*/
void setup()
{
  pinMode(topLeftMotorDirection, OUTPUT);
  pinMode(topLeftMotorPWM, OUTPUT);

  pinMode(bottomLeftMotorDirection, OUTPUT);
  pinMode(bottomLeftMotorPWM, OUTPUT);

  pinMode(topRightMotorDirection, OUTPUT);
  pinMode(topRightMotorPWM, OUTPUT);

  pinMode(bottomRightMotorDirection, OUTPUT);
  pinMode(bottomRightMotorPWM, OUTPUT);

  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  Serial.begin(115200);

}


void loop()
{
  delay(100); // Wait for checking of transmission connection
}

void receiveEvent(int howMany)
{
    while(1 < Wire.available() ) // Necessary for reading accurate values from Due
    {

      char c  = Wire.read();
      Serial.println(c);
    }

    int x = Wire.read();
    Serial.print(x);



    if (x == 10)
    {
      upMovement();
      Serial.println("Moving Forward");
    }
    else if(x == 20)
    {
      leftMovement();
      Serial.println("Moving Left");
    }
    else if(x == 30)
    {
      downMovement();
      Serial.println("Moving Reverse");
    }
    else if(x == 50)
    {
      rightMovement();
      Serial.println("Moving Right");
    }
    else if(x == 45)
    {
      allStop();
      Serial.println("Stopping Everything");
    }
    else if (x == 60)
    {
      wholeBotantiClockwise();
      Serial.println("antiClockwise Motion");
    }
    else if( x == 70)
    {
      wholeBotClockwise();
      Serial.println("Clockwise Motion");
    }
}


void leftMovement()
{
  digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(topLeftMotorPWM, 50);

  digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
  analogWrite(bottomLeftMotorPWM, 45);

  digitalWrite(bottomRightMotorDirection, HIGH);
  analogWrite(bottomRightMotorPWM, 45);

  digitalWrite(topRightMotorDirection, LOW); //
  analogWrite(topRightMotorPWM, 50);

}


void rightMovement()
{

    digitalWrite(topLeftMotorDirection,HIGH); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, 45);

    digitalWrite(bottomLeftMotorDirection, LOW); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, 50);

    digitalWrite(bottomRightMotorDirection, LOW);
    analogWrite(bottomRightMotorPWM, 50);

    digitalWrite(topRightMotorDirection, HIGH); //
    analogWrite(topRightMotorPWM, 45);

}


void upMovement()
{

    digitalWrite(topLeftMotorDirection,HIGH); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, 45);

    digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, 45);

    digitalWrite(bottomRightMotorDirection, LOW);
    analogWrite(bottomRightMotorPWM, 50);

    digitalWrite(topRightMotorDirection, LOW); //
    analogWrite(topRightMotorPWM, 50);

}

void downMovement()
{

    digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, 50);

    digitalWrite(bottomLeftMotorDirection, LOW); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, 50);

    digitalWrite(bottomRightMotorDirection, HIGH);
    analogWrite(bottomRightMotorPWM, 45);

    digitalWrite(topRightMotorDirection, HIGH); //
    analogWrite(topRightMotorPWM, 45);

}


void allStop()
{


    digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, 0);

    digitalWrite(bottomLeftMotorDirection, LOW); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, 0);

    digitalWrite(bottomRightMotorDirection, LOW);
    analogWrite(bottomRightMotorPWM, 0);

    digitalWrite(topRightMotorDirection, LOW); //
    analogWrite(topRightMotorPWM, 0);

}


void wholeBotClockwise()
{

      digitalWrite(topLeftMotorDirection,HIGH); // Left Top Motor clockwise
      analogWrite(topLeftMotorPWM, 45);

      digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
      analogWrite(bottomLeftMotorPWM, 45);

      digitalWrite(bottomRightMotorDirection, HIGH);
      analogWrite(bottomRightMotorPWM, 45);

      digitalWrite(topRightMotorDirection, HIGH); //
      analogWrite(topRightMotorPWM, 45);


}

void wholeBotantiClockwise()
{

      digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
      analogWrite(topLeftMotorPWM, 50);

      digitalWrite(bottomLeftMotorDirection, LOW); // Left Bottom Motor antiClockwise
      analogWrite(bottomLeftMotorPWM, 50);

      digitalWrite(bottomRightMotorDirection, LOW);
      analogWrite(bottomRightMotorPWM, 50);

      digitalWrite(topRightMotorDirection, LOW); //
      analogWrite(topRightMotorPWM, 50);

}
