#include <PS2X_lib.h>  //for v1.6

// Chasis Motors
/*
int topLeftMotorDirection = 25;
int topLeftMotorPWM = 4;

int bottomLeftMotorDirection = 24;
int bottomLeftMotorPWM = 5;

int bottomRightMotorDirection = 23;
int bottomRightMotorPWM = 2;

int topRightMotorDirection = 22;
int topRightMotorPWM = 3;
*/
// Speed of motor provided as PWM
int speed = 240;

//Line Sensor Structure   // From the Front side moving towards RIGHT
//LEFT Side Sensor
// ll3 ll2 ll1 lcl lcr lr1 lr2 lr3

//RIGHT Side Sensor
//rl3 rl2 rl1 rcl rcr rr1 rr2 rr3

//Line sensor LeftSide Pins

int ll3 = 22;
int ll2 = 23;
int ll1 = 24;
int lcl = 25;
int lcr = 26;
int lr1 = 27;
int lr2 = 28;
int lr3 = 29;

//Line Sensor Right Side

int rl3 = 53;
int rl2 = 52;
int rl1 = 51;
int rcl = 50;
int rcr = 49;
int rr1 = 48;
int rr2 = 47;
int rr3 = 46;


/******************************************************************
 * set pins connected to PS2 controller:
 *   - 1e column: original
 *   - 2e colmun: Stef?
 * replace pin numbers by the ones you use
 ******************************************************************/
#define PS2_DAT        53  //14
#define PS2_CMD        51  //15
#define PS2_SEL        49  //16
#define PS2_CLK        47  //17

/******************************************************************
 * select modes of PS2 controller:
 *   - pressures = analog reading of push-butttons
 *   - rumble    = motor rumbling
 * uncomment 1 of the lines for each mode selection
 ******************************************************************/
//#define pressures   true
#define pressures   false
//#define rumble      true
#define rumble      false

PS2X ps2x; // create PS2 Controller Class

//right now, the library does NOT support hot pluggable controllers, meaning
//you must always either restart your Arduino after you connect the controller,
//or call config_gamepad(pins) again after connecting the controller.

int error = 0;
byte type = 0;
byte vibrate = 0;

void setup()
{
  //pinMode Setups for chasis Motors

  pinMode(topLeftMotorDirection, OUTPUT);
  pinMode(topLeftMotorPWM, OUTPUT);

  pinMode(bottomLeftMotorDirection, OUTPUT);
  pinMode(bottomLeftMotorPWM, OUTPUT);

  pinMode(topRightMotorDirection, OUTPUT);
  pinMode(topRightMotorPWM, OUTPUT);

  pinMode(bottomRightMotorDirection, OUTPUT);
  pinMode(bottomRightMotorPWM, OUTPUT);

  //pinMode setup for Left Side Line Sensor

  pinMode(ll3, INPUT);
  pinMode(ll2, INPUT);
  pinMode(ll1, INPUT);
  pinMode(lcl, INPUT);
  pinMode(lcr, INPUT);
  pinMode(lr1, INPUT);
  pinMode(lr2, INPUT);
  pinMode(lr3, INPUT);


  //pinMode setup for Right Side Line Sensor

  pinMode(rl3, INPUT);
  pinMode(rl2, INPUT);
  pinMode(rl1, INPUT);
  pinMode(rcl, INPUT);
  pinMode(rcr, INPUT);
  pinMode(rr1, INPUT);
  pinMode(rr2, INPUT);
  pinMode(rr3, INPUT);



  Serial.begin(57600);

  delay(300);  //added delay to give wireless ps2 module some time to startup, before configuring it

  //CHANGES for v1.6 HERE!!! **************PAY ATTENTION*************

  //setup pins and settings: GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
  error = ps2x.config_gamepad(PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);

  if(error == 0){
    Serial.print("Found Controller, configured successful ");
    Serial.print("pressures = ");
    if (pressures)
      Serial.println("true ");
    else
      Serial.println("false");
    Serial.print("rumble = ");
    if (rumble)
      Serial.println("true)");
    else
      Serial.println("false");
    Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;");
    Serial.println("holding L1 or R1 will print out the analog stick values.");
    Serial.println("Note: Go to www.billporter.info for updates and to report bugs.");
  }
  else if(error == 1)
    Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");

  else if(error == 2)
    Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");

  else if(error == 3)
    Serial.println("Controller refusing to enter Pressures mode, may not support it. ");

    //  Serial.print(ps2x.Analog(1), HEX);

  type = ps2x.readType();
  switch(type) {
    case 0:
      Serial.print("Unknown Controller type found ");
      break;
    case 1:
      Serial.print("DualShock Controller found ");
      break;
    case 2:
      Serial.print("Wireless Sony DualShock Controller found ");
      break;
   }
}

void loop()
{
  /* You must Read Gamepad to get new values and set vibration values
     ps2x.read_gamepad(small motor on/off, larger motor strenght from 0-255)
     if you don't enable the rumble, use ps2x.read_gamepad(); with no values
     You should call this at least once a second
   */
  if(error == 1) //skip loop if no controller found
    return;

  if(type == 2){ //Guitar Hero Controller
    ps2x.read_gamepad();          //read controller

  }
  else { //DualShock Controller
    ps2x.read_gamepad(false, vibrate); //read controller and set large motor to spin at 'vibrate' speed






    //readLineSensorLeftSide();
    readLineSensorRightSide();





    if(ps2x.Button(PSB_START))         //will be TRUE as long as button is pressed
      Serial.println("Start is being held");
    if(ps2x.Button(PSB_SELECT))
      Serial.println("Select is being held");

    if(ps2x.ButtonPressed(PSB_PAD_UP)) {      //will be TRUE as long as button is pressed

      upMovement();

      Serial.print("Up held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_UP), DEC);
    }


    if(ps2x.ButtonReleased(PSB_PAD_UP)) {      //will be TRUE as long as button is pressed

      allStop();

      Serial.print("Up held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_UP), DEC);
    }

    if(ps2x.ButtonPressed(PSB_PAD_RIGHT)){

      rightMovement();

      Serial.print("Right held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_RIGHT), DEC);
    }


    if(ps2x.ButtonReleased(PSB_PAD_RIGHT)){

      allStop();

      Serial.print("Right held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_RIGHT), DEC);
    }

    if(ps2x.ButtonPressed(PSB_PAD_LEFT)){

    leftMovement();

      Serial.print("LEFT held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_LEFT), DEC);
    }


     if(ps2x.ButtonReleased(PSB_PAD_LEFT)){

    allStop();

      Serial.print("LEFT held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_LEFT), DEC);
    }

    if(ps2x.ButtonPressed(PSB_PAD_DOWN)){

       downMovement();

      Serial.print("DOWN held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_DOWN), DEC);
    }

    if(ps2x.ButtonReleased(PSB_PAD_DOWN)){

       allStop();

      Serial.print("DOWN held this hard: ");
      Serial.println(ps2x.Analog(PSAB_PAD_DOWN), DEC);
    }


    vibrate = ps2x.Analog(PSAB_CROSS);  //this will set the large motor vibrate speed based on how hard you press the blue (X) button
    if (ps2x.NewButtonState()) {        //will be TRUE if any button changes state (on to off, or off to on)
      if(ps2x.Button(PSB_L3))
        Serial.println("L3 pressed");
      if(ps2x.Button(PSB_R3))
        Serial.println("R3 pressed");
      if(ps2x.ButtonPressed(PSB_L2))
      {

          wholeBotantiClockwise();

        Serial.println("L2 pressed");
      }
      if(ps2x.ButtonReleased(PSB_L2))
      {

          allStop();

        Serial.println("L2 released");
      }

      if(ps2x.ButtonPressed(PSB_R2))
      {
          wholeBotClockwise();
          Serial.println("R2 pressed");
      }
      if(ps2x.ButtonReleased(PSB_R2))
      {
          allStop();
          Serial.println("R2 released");
      }

      if(ps2x.Button(PSB_TRIANGLE))
        Serial.println("Triangle pressed");
    }

    if(ps2x.ButtonPressed(PSB_CIRCLE))               //will be TRUE if button was JUST pressed
      Serial.println("Circle just pressed");
    if(ps2x.NewButtonState(PSB_CROSS))               //will be TRUE if button was JUST pressed OR released
    {
      allStop();

      Serial.println("X just changed");
    }


    if(ps2x.ButtonReleased(PSB_SQUARE))              //will be TRUE if button was JUST released
      Serial.println("Square just released");

    if(ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)) { //print stick values if either is TRUE
      Serial.print("Stick Values:");
      Serial.print(ps2x.Analog(PSS_LY), DEC); //Left stick, Y axis. Other options: LX, RY, RX
      Serial.print(",");
      Serial.print(ps2x.Analog(PSS_LX), DEC);
      Serial.print(",");
      Serial.print(ps2x.Analog(PSS_RY), DEC);
      Serial.print(",");
      Serial.println(ps2x.Analog(PSS_RX), DEC);
    }
  }
  delay(50);
}

void leftMovement()
{
  digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(topLeftMotorPWM, speed);

  digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
  analogWrite(bottomLeftMotorPWM, speed);

  digitalWrite(topRightMotorDirection, HIGH); //
  analogWrite(topRightMotorPWM, speed);

  digitalWrite(bottomRightMotorDirection, HIGH);
  analogWrite(bottomRightMotorPWM, speed);

}


void rightMovement()
{

    digitalWrite(topLeftMotorDirection,HIGH); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, speed);

    digitalWrite(bottomLeftMotorDirection, LOW); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, speed);

    digitalWrite(topRightMotorDirection, LOW); //
    analogWrite(topRightMotorPWM, speed);

    digitalWrite(bottomRightMotorDirection, LOW);
    analogWrite(bottomRightMotorPWM, speed);
}


void upMovement()
{

    digitalWrite(topLeftMotorDirection,HIGH); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, speed);

    digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, speed);

    digitalWrite(topRightMotorDirection, HIGH); //
    analogWrite(topRightMotorPWM, speed);

    digitalWrite(bottomRightMotorDirection, LOW);
    analogWrite(bottomRightMotorPWM, speed);
}

void downMovement()
{

    digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, speed);

    digitalWrite(bottomLeftMotorDirection, LOW); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, speed);

    digitalWrite(topRightMotorDirection, LOW); //
    analogWrite(topRightMotorPWM, speed);

    digitalWrite(bottomRightMotorDirection, HIGH);
    analogWrite(bottomRightMotorPWM, speed);
}


void allStop()
{


    digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
    analogWrite(topLeftMotorPWM, 0);

    digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
    analogWrite(bottomLeftMotorPWM, 0);

    digitalWrite(topRightMotorDirection, LOW); //
    analogWrite(topRightMotorPWM, 0);

    digitalWrite(bottomRightMotorDirection, HIGH);
    analogWrite(bottomRightMotorPWM, 0);
}


void wholeBotClockwise()
{

      digitalWrite(topLeftMotorDirection,HIGH); // Left Top Motor clockwise
      analogWrite(topLeftMotorPWM, speed);

      digitalWrite(bottomLeftMotorDirection, HIGH); // Left Bottom Motor antiClockwise
      analogWrite(bottomLeftMotorPWM, speed);

      digitalWrite(topRightMotorDirection, HIGH); //
      analogWrite(topRightMotorPWM, speed);

      digitalWrite(bottomRightMotorDirection, HIGH);
      analogWrite(bottomRightMotorPWM, speed);

}

void wholeBotantiClockwise()
{

      digitalWrite(topLeftMotorDirection,LOW); // Left Top Motor clockwise
      analogWrite(topLeftMotorPWM, speed);

      digitalWrite(bottomLeftMotorDirection, LOW); // Left Bottom Motor antiClockwise
      analogWrite(bottomLeftMotorPWM, speed);

      digitalWrite(topRightMotorDirection, LOW); //
      analogWrite(topRightMotorPWM, speed);

      digitalWrite(bottomRightMotorDirection, LOW);
      analogWrite(bottomRightMotorPWM, speed);
}

void readLineSensorLeftSide()
{
  Serial.println(' ');
  int LL3 = digitalRead(ll3);
  Serial.print(LL3);
  Serial.print(' ');

  int LL2 = digitalRead(ll2);
  Serial.print(LL2);
  Serial.print(' ');

  int LL1 = digitalRead(ll1);
  Serial.print(LL1);
  Serial.print(' ');

  int LCL = digitalRead(lcl);
  Serial.print(LCL);
  Serial.print(' ');

  int LCR = digitalRead(lcr);
  Serial.print(LCR);
  Serial.print(' ');

  int LR1 = digitalRead(lr1);
  Serial.print(LR1);
  Serial.print(' ');

  int LR2 = digitalRead(lr2);
  Serial.print(LR2);
  Serial.print(' ');

  int LR3 = digitalRead(lr3);
  Serial.print(LR3);
  Serial.println(' ');

}

void readLineSensorRightSide()
{
  Serial.println(' ');
  int RL3 = digitalRead(rl3);
  Serial.print(RL3);
  Serial.print(' ');

  int RL2 = digitalRead(rl2);
  Serial.print(RL2);
  Serial.print(' ');

  int RL1 = digitalRead(rl1);
  Serial.print(RL1);
  Serial.print(' ');

  int RCL = digitalRead(rcl);
  Serial.print(RCL);
  Serial.print(' ');

  int RCR = digitalRead(rcr);
  Serial.print(RCR);
  Serial.print(' ');

  int RR1 = digitalRead(rr1);
  Serial.print(RR1);
  Serial.print(' ');

  int RR2 = digitalRead(rr2);
  Serial.print(RR2);
  Serial.print(' ');

  int RR3 = digitalRead(rr3);
  Serial.print(RR3);
  Serial.println(' ');

}

void leftSideMovementWithLineSensor()
{


}

void rightSideMovementWithLineSensor()
{

}
